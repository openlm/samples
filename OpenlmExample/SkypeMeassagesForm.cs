﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SKYPE4COMLib;
using System.Threading;
using System.Collections;

namespace OpenlmExample
{
    public partial class SkypeMeassagesForm : Form
    {
        private string _userName;
        Skype skype;
        IChatMessage _ichatmessage;
        
        public SkypeMeassagesForm(string userName)
        {
            InitializeComponent();
            _userName = userName;
            
            skype = new Skype();
            _ichatmessage = null;
            // Use skype protocol version 7 
            skype.Attach(7, false);
            // Listen 
            skype.MessageStatus +=
              new _ISkypeEvents_MessageStatusEventHandler(skype_MessageStatus);
        }

        private void skype_MessageStatus(ChatMessage msg,
                     TChatMessageStatus status)
        {
            if (msg.FromHandle == _userName.ToLower())
            {
                richTxtChat.Text += _userName + " say: " + msg.Body + "\r\n";
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            SendSkypeMessage();
        }

        private void SendSkypeMessage()
        {
            Cursor.Current = Cursors.WaitCursor;
            bool sendSuccess = true;
            try
            {
                if (_ichatmessage == null)
                {

                    // We will set our Skype Class and locals with the same names but just using 
                    // lower case. This makes it easier to use this logic for the entire program.
                    // All Skype names are in lower case, make sure we are
                    string FindSkypeName = _userName.ToLower();


                    // Try and Find the "Exact" Skype name
                    IUserCollection iusercollection = skype.SearchForUsers(FindSkypeName);

                    // Did we find this "Exact" Skype name 
                    if ((iusercollection.Count > 0)
                        && (iusercollection[1].Handle == FindSkypeName))
                    {
                        // If this Skype Name is not a friend and they have no pending
                        // add friend request already, send an add request as a friend
                        if ((iusercollection[1].BuddyStatus != TBuddyStatus.budPendingAuthorization)
                            && (iusercollection[1].BuddyStatus != TBuddyStatus.budFriend))
                            iusercollection[1].BuddyStatus = TBuddyStatus.budPendingAuthorization;

                        // You can save the reference to ichatmessage so you could send
                        // more chat messages
                        _ichatmessage = skype.SendMessage(FindSkypeName, txtMessage.Text);
                    }
                    else
                    {
                        sendSuccess = false;
                        MessageBox.Show("Skype Name: " + FindSkypeName + " Not found", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    // Another message
                    _ichatmessage.Chat.SendMessage(txtMessage.Text);
                }
            }
            catch (Exception SearchError)
            {
                sendSuccess = false;
                MessageBox.Show(DateTime.Now.ToLocalTime() + ": " +
                 " Skype name and send chat message failed" +
                 " - Exception Source: " + SearchError.Source + " - Exception Message: " + SearchError.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                
                if (sendSuccess)
                {
                    richTxtChat.Text += "Me: " + txtMessage.Text + " \r\n";
                    txtMessage.Text = "";
                }
                
                
            }
        }

    }
}
