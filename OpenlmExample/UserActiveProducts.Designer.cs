﻿namespace OpenlmExample
{
    partial class UserActiveProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserActiveProducts));
            this.listViewProducts = new System.Windows.Forms.ListView();
            this.columnProduct = new System.Windows.Forms.ColumnHeader();
            this.columnServer = new System.Windows.Forms.ColumnHeader();
            this.btnOK = new System.Windows.Forms.Button();
            this.columnVendor = new System.Windows.Forms.ColumnHeader();
            this.columnProductName = new System.Windows.Forms.ColumnHeader();
            this.columnWorkstation = new System.Windows.Forms.ColumnHeader();
            this.columnStartTime = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // listViewProducts
            // 
            this.listViewProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnProduct,
            this.columnServer,
            this.columnVendor,
            this.columnProductName,
            this.columnWorkstation,
            this.columnStartTime});
            this.listViewProducts.Location = new System.Drawing.Point(12, 12);
            this.listViewProducts.Name = "listViewProducts";
            this.listViewProducts.Size = new System.Drawing.Size(814, 249);
            this.listViewProducts.TabIndex = 0;
            this.listViewProducts.UseCompatibleStateImageBehavior = false;
            this.listViewProducts.View = System.Windows.Forms.View.Details;
            // 
            // columnProduct
            // 
            this.columnProduct.Text = "Product";
            this.columnProduct.Width = 128;
            // 
            // columnServer
            // 
            this.columnServer.Text = "Server Name";
            this.columnServer.Width = 109;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnOK.Location = new System.Drawing.Point(751, 267);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // columnVendor
            // 
            this.columnVendor.Text = "Vendor";
            this.columnVendor.Width = 118;
            // 
            // columnProductName
            // 
            this.columnProductName.Text = "Product Name";
            this.columnProductName.Width = 133;
            // 
            // columnWorkstation
            // 
            this.columnWorkstation.Text = "Workstation";
            this.columnWorkstation.Width = 160;
            // 
            // columnStartTime
            // 
            this.columnStartTime.Text = "Start Time";
            this.columnStartTime.Width = 162;
            // 
            // UserActiveProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(835, 301);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.listViewProducts);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "UserActiveProducts";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "UserActiveProducts";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewProducts;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ColumnHeader columnProduct;
        private System.Windows.Forms.ColumnHeader columnServer;
        private System.Windows.Forms.ColumnHeader columnVendor;
        private System.Windows.Forms.ColumnHeader columnProductName;
        private System.Windows.Forms.ColumnHeader columnWorkstation;
        private System.Windows.Forms.ColumnHeader columnStartTime;
    }
}