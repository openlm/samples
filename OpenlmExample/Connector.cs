﻿using System.IO;

namespace OpenlmExample
{
    public abstract class Connector
    {/*
        Stream SendRequest(string xml);*/
        public abstract object GetLoginFormSettingsMessage();
        public abstract object GetActiveProductsMessage(string sessionID);
        public abstract object GetUserDescriptionMessage(string userName);
        public abstract object GetUserAuthenticationMessage(string userName, string password);
    }
}