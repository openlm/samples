﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.IO;

namespace OpenlmExample
{
    public class ServerConnector : Connector
    {
        private static ServerConnector instance;

        private int _port = 5015;
        private string _server = "localhost";

        private ServerConnector() { }

        public static ServerConnector Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServerConnector();
                }
                return instance;
            }
        }

        private Stream SendRequest(string xml)
        {
            //string url = "http://localhost:5015/api/easyadminapi/postmessage";
            string url = $"http://{_server}:{_port}/api/easyadminapi/postmessage";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            byte[] requestBytes = System.Text.Encoding.ASCII.GetBytes(xml);
            req.Method = "POST";
            req.ContentType = "text/xml;charset=utf-8";
            req.ContentLength = requestBytes.Length;
            Stream requestStream = req.GetRequestStream();
            requestStream.Write(requestBytes, 0, requestBytes.Length);
            requestStream.Close();

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);

            Stream s = new MemoryStream(ASCIIEncoding.Default.GetBytes(sr.ReadToEnd()));
            sr.Close();
            res.Close();

            return s;
        }

        private AnalyzeMessage GetMessage(string message)
        {
            Stream response = Instance.SendRequest(message);
            return new AnalyzeMessage(response);
        }

        public override object GetLoginFormSettingsMessage()
        {
            return GetMessage("<ULM><MESSAGE type=\"GetLoginFormSettings\" /><OLDSESSION /></ULM>");
        }

        public override object GetUserDescriptionMessage(string userName)
        {
            return GetMessage($"<ULM><MESSAGE type=\"GetUserDescription\" /><PARAMETERS><PARAM name=\"username\">{userName.Trim()}</PARAM></PARAMETERS></ULM>");
        }

        public override object GetUserAuthenticationMessage(string userName, string password)
        {
            return GetMessage($"<ULM><MESSAGE type=\"UserAuthentication\" /><NAME>{userName.Trim()}</NAME><PWD>{password.Trim()}</PWD></ULM>");
        }

        public override object GetActiveProductsMessage(string sessionID)
        {
            return GetMessage($"<ULM><MESSAGE type=\"GetActiveProducts\" timeformat=\"dd/mm/yyyy hh:mm:ss\" timezone=\"Israel Standard Time\" /><SESSION sessionId=\"{sessionID}\" /></ULM>");
        }
    }
}
