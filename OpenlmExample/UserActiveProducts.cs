﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OpenlmExample
{
    public partial class UserActiveProducts : Form
    {
        public UserActiveProducts(string userName, List<ActiveProductData> products)
        {
            InitializeComponent();
            this.Text = userName + " Active Products";
            AddItems(products);
        }

        private void AddItems(List<ActiveProductData> products)
        {
            int i = 0;
            Color shaded = Color.FromArgb(240, 240, 240);
            foreach (ActiveProductData pro in products)
            {
                ListViewItem item = new ListViewItem(pro.Product, 0);
                if (pro.ServerName != null && pro.ServerName.Trim() != "")
                {
                    item.SubItems.Add(pro.ServerName);
                }
                else
                {
                    item.SubItems.Add("");
                }

                if (pro.Vendor != null && pro.Vendor.Trim() != "")
                {
                    item.SubItems.Add(pro.Vendor);
                }
                else
                {
                    item.SubItems.Add("");
                }
                
                if (pro.ProductName != null && pro.ProductName.Trim() != "")
                {
                    item.SubItems.Add(pro.ProductName);
                }
                else
                {
                    item.SubItems.Add("");
                }

                if (pro.Workstation != null && pro.Workstation.Trim() != "")
                {
                    item.SubItems.Add(pro.Workstation);
                }
                else
                {
                    item.SubItems.Add("");
                }

                if (pro.StartTime != null && pro.StartTime != "")
                {
                    item.SubItems.Add(pro.StartTime.ToString());
                }
                else
                {
                    item.SubItems.Add("");
                }
                
                if (i++ % 2 == 1)
                {
                    item.BackColor = shaded;
                    item.UseItemStyleForSubItems = true;
                }

                listViewProducts.Items.Add(item);
            }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
