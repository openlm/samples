﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using OpenlmExample.SaasApi;

namespace OpenlmExample
{
    public partial class UserAuthentication : Form
    {
        private Mode mode;
        private Connector connector;

        public UserAuthentication(Connector connector)
        {
            InitializeComponent();
            this.connector = connector;
            mode = Mode.Local;
        }

        public UserAuthentication()
        {
            InitializeComponent();
            mode = Mode.Saas;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (mode == Mode.Local)
            {
                AnalyzeMessage analyze = connector.GetUserAuthenticationMessage(txtUserName.Text, txtPwd.Text) as AnalyzeMessage;
                string messageType = analyze.MessageType;

                //XML format - <ULM><MESSAGE type="SessionAuthenticationID" /><SESSIONID>15122010162704354</SESSIONID></ULM>
                if (messageType == "SessionAuthenticationID")
                {
                    MessageBox.Show("Success", "success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //set user session id
                    Openlm op = (Openlm)this.Owner;
                    op.SetUserSessionID(analyze.AnalyzeUserSessionIdAuthenticationProcess());
                    this.Close();
                }
                //XML format - <ULM><MESSAGE type="Error">Invalid Username or Password</MESSAGE></ULM>
                else if (messageType == "Error")
                {
                    MessageBox.Show(analyze.AnalyzeErrorMessageReason().Trim(), "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.txtUserName.Focus();
                }
            }
            else
            {
                var saasConnector = new SaasConnector();
                var userAuthMessage = saasConnector.GetUserAuthenticationMessage(txtUserName.Text, txtPwd.Text) as LoginResponse;
                if (userAuthMessage!=null&&!string.IsNullOrWhiteSpace(userAuthMessage.SaasToken))
                {
                    MessageBox.Show("Success", "success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Error", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.txtUserName.Focus();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
