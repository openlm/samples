﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace OpenlmExample
{
    [XmlRoot("ULM")]
    public class ActiveProduct
    {
        [System.Xml.Serialization.XmlElement("MESSAGE", IsNullable = true)]
        public Products Data { get; set; }
    }

    public class Products
    {
        [System.Xml.Serialization.XmlArray("PRODUCTS", IsNullable = true)]
        [System.Xml.Serialization.XmlArrayItem("PRODUCT")]
        public List<ActiveProductData> ActiveProductDataList { get; set; }
    }
    
    public class ActiveProductData
    {
       [System.Xml.Serialization.XmlAttribute("username")]
        public string UserName { get; set; }

        [System.Xml.Serialization.XmlAttribute("product")]
        public string Product { get; set; }

        [System.Xml.Serialization.XmlAttribute("server_name")]
        public string ServerName { get; set; }

        [System.Xml.Serialization.XmlAttribute("vendor")]
        public string Vendor { get; set; }

        [System.Xml.Serialization.XmlAttribute("product_name")]
        public string ProductName { get; set; }

        [System.Xml.Serialization.XmlAttribute("workstation")]
        public string Workstation { get; set; }

        string _startTime;
        
        [System.Xml.Serialization.XmlAttribute("start_time")]
        public string StartTime
        {
            get
            {
                return Tools.Num2Date(int.Parse(_startTime), false).ToString();
            }
            
            set
            {
                _startTime = value;
            }
        }
    }
}
