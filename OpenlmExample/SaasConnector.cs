﻿using OpenlmExample.SaasApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenlmExample
{
    class SaasConnector : Connector
    {
        private static SaasClient instance;
        private static string SaasToken;

        public SaasConnector()
        {
        }

        public static SaasClient Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SaasApi.SaasClient();
                }
                return instance;
            }
        }

        public override object GetLoginFormSettingsMessage()
        {
            return true;
        }

        public override object GetUserDescriptionMessage(string userName)
        {
            return null;// GetMessage($"<ULM><MESSAGE type=\"GetUserDescription\" /><PARAMETERS><PARAM name=\"username\">{userName.Trim()}</PARAM></PARAMETERS></ULM>");
        }

        public override object GetUserAuthenticationMessage(string userName, string password)
        {
            try
            {
                var loginResponse = Instance.SaasLogIn(new SaasLoginRequest()
                {
                    Username = userName,
                    Password = password
                });
                SaasToken = loginResponse.SaasToken;
                return loginResponse;

            }
            catch
            {
                return null;
            }
        }

        public override object GetActiveProductsMessage(string sessionID)
        {
            var client = new SaasApi.SaasClient();

            var startTime = DateTime.UtcNow.Date.AddDays(-30);
            var endTime = DateTime.UtcNow;

            var request = new LicensesActivityRequest
            {
                SaasToken = SaasToken,
                BaseInfo = new RequestBaseInfo
                {
                    PagingData = new PagingData
                    {
                        StartRecord = 0,
                        NumOfRecord = 10,
                        Sort = new string[] { "usagetime" },
                        Direction = "desc"
                    },

                    UserLocalSettings = new UserLocalSettings
                    {
                        TimezoneStandardName = "UTC",
                        ThousandsSeparator = ",",
                        DecimalSeparator = ".",
                        TimeFormat = "dd/mm/yyyy hh:mm:ss"
                    }
                },

                GroupBy = "User",
                StartTime = new SlimDateTime()
                {
                    Year = startTime.Year,
                    Month = startTime.Month,
                    Day = startTime.Day
                },

                EndTime = new SlimDateTime()
                {
                    Year = endTime.Year,
                    Month = endTime.Month,
                    Day = endTime.Day,
                    Hour = endTime.Hour,
                    Minute = endTime.Minute,
                    Second = endTime.Second
                }
            };

            var response = client.GetLicensesActivityByGroup(request);
            List<string> returnValue = new List<string>();
            foreach (var value in response.LicensesActivities)
            {
                returnValue.Add(value.GroupByField);
            }
            return returnValue;
        }
    }
}
