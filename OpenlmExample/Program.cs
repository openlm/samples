﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OpenlmExample
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Mode mode;
            if (args.Any()&&args[0].Trim().ToLower()=="local")
            {
                mode = Mode.Local;
            }
            else
            {
                mode = Mode.Saas;
            }
            Application.Run(new Openlm(mode));
        }
    }
    public enum Mode
    {
        Local,
        Saas
    }
}
