﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenlmExample
{
    public static class Tools
    {
        public static DateTime Num2Date(double number, bool returnUTCTime)
        {
            DateTime jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0);
            DateTime utcTime = DateTime.SpecifyKind(jan1st1970.AddSeconds(number), DateTimeKind.Utc);
            DateTime retVal = (returnUTCTime ? utcTime : utcTime.ToLocalTime());
            return retVal; 
        }
    }
}
